package com.chrisslinkman.app;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.chrisslinkman.app.flow.MainDispatcher;
import com.chrisslinkman.app.flow.MainKeyParceler;
import com.chrisslinkman.app.logic.screen.LandingScreen;

import flow.Flow;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context context) {
        context = Flow.configure(context, this)
                .dispatcher(new MainDispatcher(this))
                .keyParceler(new MainKeyParceler())
                .defaultKey(new LandingScreen())
                .install();
        super.attachBaseContext(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

}
