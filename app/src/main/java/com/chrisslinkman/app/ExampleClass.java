package com.chrisslinkman.app;

public class ExampleClass {

    private int first;

    public ExampleClass(int first) {
        this.first = first;
    }

    public boolean isLarger(int num){
        return num > first;
    }

}
