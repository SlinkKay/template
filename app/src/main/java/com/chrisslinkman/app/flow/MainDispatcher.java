package com.chrisslinkman.app.flow;

import android.app.Activity;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chrisslinkman.app.logic.landing.DaggerLandingComponent;
import com.chrisslinkman.app.logic.landing.LandingModule;
import com.chrisslinkman.app.logic.screen.LandingScreen;
import com.chrisslinkman.app.R;

import flow.Dispatcher;
import flow.Flow;
import flow.Traversal;
import flow.TraversalCallback;

public class MainDispatcher implements Dispatcher {

    private final Activity activity;

    public MainDispatcher(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void dispatch(@NonNull Traversal traversal, @NonNull TraversalCallback callback) {
        Object destKey = traversal.destination.top();
        ViewGroup frame = (ViewGroup) activity.findViewById(R.id.flow_frame);

        if (frame == null) {
            return;
        }

        if (frame.getChildCount() > 0) { //has children, save state
            final View currentView = frame.getChildAt(0);
            if (traversal.origin != null) {
                traversal.getState(traversal.origin.top()).save(currentView);
            }
            final Object currentKey = Flow.getKey(currentView);
            if (destKey.equals(currentKey)) {
                callback.onTraversalCompleted();
                return;
            }
            frame.removeAllViews();
        }

        @LayoutRes final int newLayout;
        if (destKey instanceof LandingScreen) {
            newLayout = R.layout.preference_layout;
        } else {
            throw new AssertionError("Unknown screen: " + destKey);
        }

        View newView = LayoutInflater.from(traversal.createContext(destKey, activity))
                .inflate(newLayout, frame, false);
        frame.addView(newView);
        traversal.getState(destKey).restore(newView);
        callback.onTraversalCompleted();
    }
}

