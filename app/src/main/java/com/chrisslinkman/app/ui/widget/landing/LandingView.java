package com.chrisslinkman.app.ui.widget.landing;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chrisslinkman.app.logic.landing.DaggerLandingComponent;
import com.chrisslinkman.app.logic.landing.LandingModule;
import com.chrisslinkman.app.logic.landing.LandingPresenter;
import com.chrisslinkman.app.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LandingView extends LinearLayout implements LandingPresenter.View {

    @BindView(R.id.user_state)
    protected TextView preferenceState;

    @Inject
    protected LandingPresenter presenter;

    public LandingView(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        DaggerLandingComponent.builder()
                .landingModule(new LandingModule())
                .build().inject(this);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        presenter.takeView(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        presenter.dropView(this);
    }

    @Override
    public void setState(String state) {
        preferenceState.setText(state);
    }
}
