package com.chrisslinkman.app.provider.model;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class UserPreference {

    public abstract boolean isSelected();

    public static Builder builder(){
        return new AutoValue_UserPreference.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder{
        public abstract Builder setSelected(boolean isSelected);

        public abstract UserPreference build();
    }
}
