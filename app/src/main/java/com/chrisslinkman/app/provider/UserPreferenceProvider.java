package com.chrisslinkman.app.provider;

import com.chrisslinkman.app.logic.landing.LandingPresenter;

public interface UserPreferenceProvider {

    void register(LandingPresenter presenter);

    void unregister(LandingPresenter presenter);

    void requestPreference();
}
