package com.chrisslinkman.app.logic.landing;

import com.chrisslinkman.app.provider.UserPreferenceProvider;
import com.chrisslinkman.app.provider.model.UserPreference;

import javax.inject.Inject;

public class LandingPresenter {

    private View view;
    private UserPreferenceProvider userPreferenceProvider;

    public interface View{
        void setState(String state);
    }

    @Inject
    LandingPresenter(UserPreferenceProvider userPreferenceProvider){
        this.userPreferenceProvider = userPreferenceProvider;
    }

    public void takeView(View view){
        this.view = view;
        userPreferenceProvider.register(this);
        userPreferenceProvider.requestPreference();
    }

    public void dropView(View view){
        if (this.view == view){
            this.view = null;
        }
    }

    public void onPreference(UserPreference preference){
        view.setState(Boolean.toString(preference.isSelected()));
    }
}
