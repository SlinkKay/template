package com.chrisslinkman.app.logic.landing;

import com.chrisslinkman.app.ui.widget.landing.LandingView;

import dagger.Component;

@Component(modules = LandingModule.class)
public interface LandingComponent {

    void inject(LandingView view);

    @Component.Builder
    interface Builder{
        Builder landingModule(LandingModule module);
        LandingComponent build();
    }
}
