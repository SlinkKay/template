package com.chrisslinkman.app.logic.landing;

import com.chrisslinkman.app.provider.UserPreferenceProvider;
import com.chrisslinkman.app.provider.model.UserPreference;

import dagger.Module;
import dagger.Provides;

@Module
public class LandingModule {

    @Provides
    UserPreferenceProvider userPreferenceProvider(){
        return new UserPreferenceProvider() {
            LandingPresenter presenter;
            @Override
            public void register(LandingPresenter presenter) {
                this.presenter = presenter;
            }

            @Override
            public void unregister(LandingPresenter presenter) {
                this.presenter = null;
            }

            @Override
            public void requestPreference() {
                if(this.presenter == null){
                    return;
                }
                presenter.onPreference(UserPreference.builder().setSelected(true).build());

            }
        };
    }

}
