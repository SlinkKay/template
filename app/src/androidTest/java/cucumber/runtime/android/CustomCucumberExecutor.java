package cucumber.runtime.android;

import android.app.Instrumentation;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import cucumber.api.StepDefinitionReporter;
import cucumber.api.java.ObjectFactory;
import cucumber.runtime.Backend;
import cucumber.runtime.ClassFinder;
import cucumber.runtime.CucumberException;
import cucumber.runtime.Env;
import cucumber.runtime.Runtime;
import cucumber.runtime.RuntimeOptions;
import cucumber.runtime.RuntimeOptionsFactory;
import cucumber.runtime.io.ResourceLoader;
import cucumber.runtime.java.JavaBackend;
import cucumber.runtime.java.ObjectFactoryLoader;
import cucumber.runtime.model.CucumberFeature;
import dalvik.system.DexFile;
import gherkin.formatter.Formatter;
import gherkin.formatter.Reporter;

/**
 * Light customization on the {@link CucumberExecutor} for the executor to get a direct target
 * for the {@link cucumber.api.CucumberOptions} file.
 */
public class CustomCucumberExecutor {
    private final Instrumentation instrumentation;
    private final ClassLoader classLoader;
    private final ClassFinder classFinder;
    private final RuntimeOptions runtimeOptions;
    private final Runtime runtime;
    private final List<CucumberFeature> cucumberFeatures;

    public CustomCucumberExecutor(Arguments arguments, Instrumentation instrumentation, Class target) {
        this.trySetCucumberOptionsToSystemProperties(arguments);
        Context context = instrumentation.getContext();
        this.instrumentation = instrumentation;
        this.classLoader = context.getClassLoader();
        this.classFinder = this.createDexClassFinder(context);
        this.runtimeOptions = this.createRuntimeOptions(context, target);
        ResourceLoader resourceLoader = new AndroidResourceLoader(context);
        this.runtime = new Runtime(resourceLoader, this.classLoader, this.createBackends(), this.runtimeOptions);
        this.cucumberFeatures = this.runtimeOptions.cucumberFeatures(resourceLoader);
    }

    public void execute() {
        this.runtimeOptions.addPlugin(new AndroidInstrumentationReporter(this.runtime, this.instrumentation, this.getNumberOfConcreteScenarios()));
        this.runtimeOptions.addPlugin(new AndroidLogcatReporter(this.runtime, "cucumber-android"));
        Reporter reporter = this.runtimeOptions.reporter(this.classLoader);
        Formatter formatter = this.runtimeOptions.formatter(this.classLoader);
        StepDefinitionReporter stepDefinitionReporter = this.runtimeOptions.stepDefinitionReporter(this.classLoader);
        this.runtime.getGlue().reportStepDefinitions(stepDefinitionReporter);
        Iterator var4 = this.cucumberFeatures.iterator();

        while(var4.hasNext()) {
            CucumberFeature cucumberFeature = (CucumberFeature)var4.next();
            cucumberFeature.run(formatter, reporter, this.runtime);
        }

        formatter.done();
        formatter.close();
    }

    public int getNumberOfConcreteScenarios() {
        return ScenarioCounter.countScenarios(this.cucumberFeatures);
    }

    private void trySetCucumberOptionsToSystemProperties(Arguments arguments) {
        String cucumberOptions = arguments.getCucumberOptions();
        if(!cucumberOptions.isEmpty()) {
            Log.d("cucumber-android", "Setting cucumber.options from arguments: '" + cucumberOptions + "'");
            System.setProperty("cucumber.options", cucumberOptions);
        }

    }

    private ClassFinder createDexClassFinder(Context context) {
        String apkPath = context.getPackageCodePath();
        return new DexClassFinder(this.newDexFile(apkPath));
    }

    private DexFile newDexFile(String apkPath) {
        try {
            return new DexFile(apkPath);
        } catch (IOException var3) {
            throw new CucumberException("Failed to open " + apkPath);
        }
    }

    private RuntimeOptions createRuntimeOptions(Context context, Class<?> clazz) {
        Log.d("cucumber-android", "Found CucumberOptions in class " + clazz.getName());
        RuntimeOptionsFactory factory = new RuntimeOptionsFactory(clazz);
        return factory.create();
    }

    private Collection<? extends Backend> createBackends() {
        ObjectFactory delegateObjectFactory = ObjectFactoryLoader.loadObjectFactory(this.classFinder, Env.INSTANCE.get(ObjectFactory.class.getName()));
        AndroidObjectFactory objectFactory = new AndroidObjectFactory(delegateObjectFactory, this.instrumentation);
        List<Backend> backends = new ArrayList();
        backends.add(new JavaBackend(objectFactory, this.classFinder));
        return backends;
    }
}
