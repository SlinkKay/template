package cucumber.api.android;

import android.app.Instrumentation;
import android.os.Bundle;
import android.os.Looper;

import cucumber.runtime.android.Arguments;
import cucumber.runtime.android.CoverageDumper;
import cucumber.runtime.android.DebuggerWaiter;
import cucumber.runtime.android.CustomCucumberExecutor;

public class CustomCucumberInstrumentationCore {

    private static final String REPORT_VALUE_ID = CustomCucumberInstrumentationCore.class.getSimpleName();
    private final Instrumentation instrumentation;
    private DebuggerWaiter debuggerWaiter;
    private CoverageDumper coverageDumper;
    private CustomCucumberExecutor cucumberExecutor;
    private Arguments arguments;
    private Class<?> target;

    /**
     *
     * @param instrumentation Instrumentation used for Android
     * @param target The {@link cucumber.api.CucumberOptions} class which has the parameters
     *               needed.
     */
    public CustomCucumberInstrumentationCore(Instrumentation instrumentation, Class<?> target) {
        this.instrumentation = instrumentation;
        this.target = target;
    }

    public void create(Bundle bundle) {
        this.arguments = new Arguments(bundle);
        this.cucumberExecutor = new CustomCucumberExecutor(this.arguments, this.instrumentation, target);
        this.coverageDumper = new CoverageDumper(this.arguments);
        this.debuggerWaiter = new DebuggerWaiter(this.arguments);
    }

    public void start() {
        Looper.prepare();
        Bundle results = new Bundle();
        if(this.arguments.isCountEnabled()) {
            results.putString("id", REPORT_VALUE_ID);
            results.putInt("numtests", this.cucumberExecutor.getNumberOfConcreteScenarios());
        } else {
            this.debuggerWaiter.requestWaitForDebugger();
            this.cucumberExecutor.execute();
            this.coverageDumper.requestDump(results);
        }

        this.instrumentation.finish(-1, results);
    }
}
