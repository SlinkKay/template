package com.chrisslinkman.app.cucumber.steps;

import android.app.Activity;
import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.chrisslinkman.app.MainActivity;
import com.chrisslinkman.app.R;
import com.chrisslinkman.app.test.ActivityFinisher;

import org.junit.Rule;
import org.junit.runner.RunWith;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertNotNull;

@SuppressWarnings("JUnitTestCaseWithNoTests")
@RunWith(AndroidJUnit4.class)
public class HelloSteps {

    private Activity mActivity;

    @Rule
    private ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class,
            false, false);

    @Before
    public void setUp() throws Exception {
        System.out.println("Setup");
        mActivity = mActivityRule.launchActivity(new Intent()); // Start Activity before each test scenario
        assertNotNull(mActivity);
    }

    @After
    public void tearDown(){
        ActivityFinisher.finishOpenActivities();
    }

    @Given("^I see the start page")
    public void start_page(){

    }

    @Then("^The text says (.*)$")
    public void checkHelloWorld(String value){
        onView(withId(R.id.user_state)).check(matches(withText(value)));
    }
}
