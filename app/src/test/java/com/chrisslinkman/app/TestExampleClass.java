package com.chrisslinkman.app;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

public class TestExampleClass {

    int first = 42;
    ExampleClass clazz;

    @Before
    public void setup(){
        clazz =  new ExampleClass(first);
    }

    @Test
    public void testSmaller(){
        //execution
        boolean bool = clazz.isLarger(13);

        //verification
        Assert.assertFalse(bool);
    }

    @Test
    public void testLarger(){
        //execution
        boolean bool = clazz.isLarger(45);

        //verification
        Assert.assertTrue(bool);
    }

}
